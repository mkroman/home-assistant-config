# Copyright (C) 2019 Mikkel Kroman <mk@maero.dk>
FROM homeassistant/home-assistant:latest

COPY ./home-assistant/ /config/
